import React from 'react';
import {View, StyleSheet, Text} from 'react-native';

// import { Container } from './styles';

export default function App() {
  return (
    <View style={styles.teste}>
      <Text style={styles.texto}>ALGUEM AQUI</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  teste: {
    backgroundColor: 'crimson',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  texto: {
    color: 'white',
  },
});
